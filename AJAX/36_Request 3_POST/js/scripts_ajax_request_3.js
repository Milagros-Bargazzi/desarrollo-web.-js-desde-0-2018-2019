const form = document.getElementById('form')
const characters = document.getElementById('characters')
const table = document.getElementById('table')

form.addEventListener('submit', (e) => {
    e.preventDefault(); //evita que se recargue la página al neviar el form
    sendData(form)
})

const sendData = (data) => {
    let xhr
    if (window.XMLHttpRequest) xhr = new XMLHttpRequest()
    else xhr = new ActiveXObject("Microsoft.XMLHTTP")

    //Abre la conexión con una petición POST
    xhr.open('POST', 'marvel.php')

    //FormData es un objeto que puede recibir datos como parámetros
    //Se le envia el form como dato
    const formData = new FormData(data)

    //Envio la petición
    xhr.send(formData);
}