const form = document.getElementById('form')
const characters = document.getElementById('characters')
const table = document.getElementById('table')


form.addEventListener('submit', (e) => {
    e.preventDefault()
    getData(characters.children[characters.selectedIndex].value)
})


//El objetivo de esta función es que aparezcan en el select todos los nombres de los super heroes
//Recibe el id que seleccionamos en el select
const getData = (id) => {
    let xhr    
    if (window.XMLHttpRequest) xhr = new XMLHttpRequest() //Creo el objeto xhr si lo soporta
    else xhr = new ActiveXObject("Microsoft.XMLHTTP") //Sino crea el de internet explorer

    if (id == undefined) { //Evalua si id existe para saber si estamos cargando la pag por primera vez o no. True si es la primera vez o sea si es undefined
        
        //Abre la conexión con una petición GET
        xhr.open('GET', 'marvel.php')

        //Escucha la respuesta
        xhr.addEventListener('load', (data) => {
            const dataJSON = JSON.parse(data.target.response) //Transforma data a json
            console.log(dataJSON)
            
            //Creamos un fragmento de código
            const fragment = document.createDocumentFragment()

            //Rellenamos el fragmento de código con el id y nombre de los heroes
            for (const heroes of dataJSON) {
                const option = document.createElement('OPTION')
                option.setAttribute('value', heroes.ID)
                option.textContent = heroes.Name
                fragment.appendChild(option)
            }

            characters.appendChild(fragment)
        })
    } else {
        //Cuando aparecen los distintos heroes en el select significa que ya cada uno tiene su id.
        //El archivo php necesita saber que id seleccionamos
        //La forma de pasar parámetros por GET es con el símbolo ?
        //Todo lo que vaya después de ? debe ir en forma de clave (id) y valor ${id}
        //El id se envia por parámetro cuando se hace sumbit al botón Get Data
        xhr.open('GET', `marvel.php?id=${id}`)

        xhr.addEventListener('load', (data) => {
            const dataJSON = JSON.parse(data.target.response)
            console.log(dataJSON)

            const fragment = document.createDocumentFragment()

            //Añade una fila con sus correspondientes columnas por cada superheroe que coincida con la búsqueda
            for (const heroe of dataJSON) {
                const row = document.createElement('TR')
                const dataName = document.createElement('TD')
                const dataAlignment = document.createElement('TD')
                const dataHometown = document.createElement('TD')
                const dataGender = document.createElement('TD')
                const dataFighting = document.createElement('TD')
                dataName.textContent = heroe.Name
                dataAlignment.textContent = heroe.Alignment
                dataHometown.textContent = heroe.Hometown
                dataGender.textContent = heroe.Gender
                dataFighting.textContent = heroe.Fighting_Skills

                row.append(dataName)
                row.append(dataAlignment)
                row.append(dataHometown)
                row.append(dataGender)
                row.append(dataFighting)

                fragment.append(row)
            }

            //Remueve la fila si es que hay una ya creada
            //Si este if no estuviera se irian agregando las filas a medida que seleccionamos los heroes. O sea que si seleccionamos varias veces el mismo se repetirian las filas
            if (table.children[1]) {
                table.removeChild(table.children[1])
            }
            table.append(fragment)
        })
    }

    //Envio la petición
    xhr.send();
}

getData();