<?php
    $conn = NULL;
        try{

            //Crear la conexión
            //1a) mysql:host=localhost-> usamos mysql y que nos conectamos al host llamado localhost
            //1b) dbname=marvel-> nombre de la db
            //1c) charset=utf8-> tipo de codificado
            //2) root-> nombre del usuario
            //3) contraseña -> '' significa sin contraseña
            $con = new PDO("mysql:host=localhost; dbname=marvel; charset=utf8", 'root', '');

            if(isset($_GET['id'])){ //evalua si estamos enviando un id
                $id = $_GET['id'];
                $sql = "SELECT * FROM characters WHERE id=$id"; //arma la consulta y la guarda en variable
            }else{
                $sql = "SELECT * FROM characters";
            }

            //Preparo la consulta
            $stm=$con->prepare($sql);

            //Ejecuto la consulta
            $stm->execute();

            //Guardo todos los resultados en un array asociativo
            $resultSet = $stm->fetchAll(PDO::FETCH_ASSOC);

            //Convierto el array asociativo a formato json para que js lo entienda
            echo json_encode($resultSet);
            
        }catch (PDOException $e){
            //Hay un echo en el caso de haber algún error
            echo "Error ".$e->getMessage();
        }


