const button = document.getElementById("button");

button.addEventListener("click", () => {
    
    let xhr

    if(window.XMLHttpRequest){

        //code for modern browsers
        xhr = new XMLHttpRequest(); //se guarda la respuesta en un objeto que usualmente se llama xhr
    }
    else
    {
        //code for old IE browsers
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    //Abrimos la conexión
    xhr.open('GET', 'https://jsonplaceholder.typicode.com/users');

    //Lo que queremos que haga con los datos que traemos
    //Tenemos que escuchar un evento para saber cuando llega la info ya que es algo asíncrono
    //El evento load se dispara cuando toda la info ha llegado al objeto.
    //Antes no existia el evento load sino que había que chequear el estado y el status de la petición.
    //Una vez que llego toda la info tenemos que guardarla en una variable -> data
    //De data lo que nos interesa es el target y de ahí dentro la response
    /*El problema es que si chequeamos con typeof la response es un string y nosotros necesitamos un objeto
    con el que poder trabajar, recorrerlo, acceder a sus propiedades, etc. Entonces tenemos que convertir el string
    a formato JSON con JSON.parse().*/
    xhr.addEventListener('load', (data) => {
        
        console.log(JSON.parse(data.target.response));
        
        const dataJSON = JSON.parse(data.target.response);

        const list = document.getElementById("list");

        //Recorre cada uno de los usuarios
        for(const userInfo of dataJSON)
        {
            const listItem = document.createElement('LI');
            listItem.textContent = `${userInfo.id} - ${userInfo.name}`;
            list.appendChild(listItem);
        }
    });

    //Enviamos la petición
    xhr.send();

});